package com.hcl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week6day5Application {

	public static void main(String[] args) {
		SpringApplication.run(Week6day5Application.class, args);
	}

}
