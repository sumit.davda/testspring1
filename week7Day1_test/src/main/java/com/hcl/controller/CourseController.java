package com.hcl.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CourseController {

	@GetMapping("/welcome/{name}")
	public String getWelcomeMessage(@PathVariable String name) {
		return "welcome " + name + "to Great Learning..."
	}
}
